package logger

import "fmt"

// CMsg shorthand for creating a ColoredMessage struct
func CMsg(foreground, background int, msg interface{}) ColoredMessage {
	return ColoredMessage{
		Color: ColorConfig{
			Foreground: foreground,
			Background: background,
		},
		Message: fmt.Sprint(msg),
	}
}

// CCMsg shorthand for creating a ColoredMessage struct using a ColorConfig struct
func CCMsg(color ColorConfig, msg interface{}) ColoredMessage {
	return ColoredMessage{
		Color:   color,
		Message: fmt.Sprint(msg),
	}
}

// CFMsg shorthand for creating a ColoredMessage struct with only a foreground color
func CFMsg(foreground int, msg interface{}) ColoredMessage {
	return ColoredMessage{
		Color: ColorConfig{
			Foreground: foreground,
			Background: ColorNone,
		},
		Message: fmt.Sprint(msg),
	}
}

// CBMsg shorthand for creating a ColoredMessage struct with only a background color
func CBMsg(background int, msg interface{}) ColoredMessage {
	return ColoredMessage{
		Color: ColorConfig{
			Foreground: ColorDependBackground,
			Background: background,
		},
		Message: fmt.Sprint(msg),
	}
}

// SMsg will create a ColoredMessage struct which will not print in color (use default color)
func SMsg(msg interface{}) ColoredMessage {
	return CMsg(ColorNone, ColorNone, msg)
}

// LMsg will create a ColoredMessage struct which will use the color of the log level
func LMsg(msg interface{}) ColoredMessage {
	return CMsg(ColorLogLevel, ColorLogLevel, msg)
}
