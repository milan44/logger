package logger

import (
	"fmt"
	"github.com/gookit/color"
	"math"
	"math/rand"
	"strconv"
)

// Color prints various ColoredMessage structs
func (l *Logger) Color(cs ...ColoredMessage) {
	for _, c := range cs {
		msg := c.Message

		if l.outputFile != nil && l.Output == l.outputFile && !l.coloredFile {
			_ = l.RawPrint(msg)
			continue
		}

		if c.Color.Foreground != ColorNone && l.UseColors {
			msg = color.RenderCode("38;5;"+strconv.Itoa(c.Color.Foreground), msg)
		}
		if c.Color.Background != ColorNone && l.UseColors {
			msg = color.RenderCode("48;5;"+strconv.Itoa(c.Color.Background), msg)
		}

		if l.Output != nil {
			color.Fprint(l.Output, msg)
		}

		_, _ = color.Reset()
	}
}

func (l *Logger) RawPrint(raw string) error {
	if l.Output != nil {
		_, err := fmt.Fprint(l.Output, raw)
		return err
	}
	return nil
}

// RandomColor returns a random color
func RandomColor() int {
	r, b, g := rand.Intn(6), rand.Intn(6), rand.Intn(6)
	return RGB2C(r, g, b)
}

// RGB2C converts rgb values (between 0 - 5) to an ANSI color index
func RGB2C(r, g, b int) int {
	r = _limit(r)
	g = _limit(g)
	b = _limit(b)

	return (36*r + 6*g + b) + 16
}

// RGB8Bit2C converts a normal 8 bit rgb color to an ANSI color index
func RGB8Bit2C(r, g, b uint8) int {
	return RGB2C(convert(r), convert(g), convert(b))
}

func convert(i uint8) int {
	return int(math.Floor((float64(i) / 256) * 6))
}
func _limit(i int) int {
	if i < 0 {
		return 0
	} else if i > 5 {
		return 5
	}
	return i
}
