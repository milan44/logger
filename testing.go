package logger

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"math"
	"math/rand"
	"net/http"
	"strconv"
)

func pad(c int) string {
	cs := fmt.Sprint(c)

	if c < 10 {
		return " " + cs + " "
	}
	if c < 100 {
		cs = " " + cs
	}

	return cs
}

func printColorBlock(c int, big bool, logger *Logger) {
	msg := pad(c)

	logger.Color(ColoredMessage{
		Color: ColorConfig{
			Foreground: ColorDependBackground,
			Background: c,
		}.PrepareDefault(logger),
		Message: " " + msg + " ",
	})

	if big {
		fmt.Print(" ")
	}
}
func printRGBBlock(rgb string, c int, logger *Logger) {
	logger.Color(ColoredMessage{
		Color: ColorConfig{
			Foreground: ColorDependBackground,
			Background: c,
		}.PrepareDefault(logger),
		Message: rgb,
	})
}
func newline(count int) {
	for i := 0; i < count; i++ {
		fmt.Println(" ")
	}
}

type colorDescriptor struct {
	c   int
	rgb string
}

func printColorTable(logger *Logger) {
	newline(1)
	for c := 0; c < 16; c++ {
		printColorBlock(c, true, logger)
	}

	linesA := make([][]colorDescriptor, 0)
	linesB := make([][]colorDescriptor, 0)

	for g := 0; g < 6; g++ {
		lineA := make([]colorDescriptor, 0)
		lineB := make([]colorDescriptor, 0)

		for b := 0; b < 6; b++ {
			for r := 0; r < 6; r++ {
				c := colorDescriptor{
					c:   RGB2C(r, g, b),
					rgb: rgb(r, g, b),
				}

				if b < 3 {
					lineA = append(lineA, c)
				} else {
					lineB = append(lineB, c)
				}
			}
		}

		linesA = append(linesA, lineA)
		linesB = append(linesB, lineB)
	}

	lines := append(linesA, linesB...)
	for li, line := range lines {
		if math.Mod(float64(li), 6) == 0 {
			newline(1)
		}

		for x := 0; x < 2; x++ {
			newline(1)
			fmt.Print(" ")

			for i, c := range line {
				if math.Mod(float64(i), 6) == 0 {
					fmt.Print(" ")
				}

				if x == 0 {
					printColorBlock(c.c, false, logger)
				} else if x == 1 {
					printRGBBlock(c.rgb, c.c, logger)
				}
			}
		}
	}

	for c := 232; c < 256; c++ {
		if math.Mod(float64(c-232), 12) == 0 {
			newline(2)
			fmt.Print("      ")
		}

		fmt.Print(" ")
		printColorBlock(c, true, logger)
	}
	newline(2)
}

func rgb(r, g, b int) string {
	return " " + strconv.Itoa(r) + strconv.Itoa(g) + strconv.Itoa(b) + " "
}

func cycleRequests() {
	gin.SetMode(gin.ReleaseMode)

	m := QuickGinMiddleware()
	c, _ := gin.CreateTestContext(discardWriter{})
	c.Request, _ = http.NewRequest("GET", "http://localhost/test/path", nil)

	codes := []int{
		100,
		200,
		300,
		400,
		500,
		218,
		418,
		420,
		999,
	}

	for i, method := range []string{"GET", "HEAD", "POST", "PUT", "DELETE", "CONNECT", "OPTIONS", "TRACE", "PATCH"} {
		c.Request.Method = method

		c.Request.RemoteAddr = fmt.Sprintf("%d.%d.%d.%d:%d", rand.Intn(256), rand.Intn(256), rand.Intn(256), rand.Intn(256), rand.Intn(65535))
		c.Status(codes[i])

		m(c)
	}
}

type discardWriter struct{}

func (d discardWriter) Write(b []byte) (int, error) {
	return len(b), nil
}
func (d discardWriter) WriteHeader(_ int) {}
func (d discardWriter) Header() http.Header {
	return http.Header{
		"user-agent": {"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0"},
	}
}
