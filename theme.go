package logger

const (
	ColorNone             = -1
	ColorLogLevel         = -2
	ColorDefault          = -3
	ColorDependBackground = -4

	ConfigTypeMessage = 0
	ConfigTypeLevel   = 1
)

// ColorConfig defines a part of a ColorTheme
type ColorConfig struct {
	Foreground int
	Background int
}

type ColorTheme struct {
	LevelDebug   ColorConfig
	LevelInfo    ColorConfig
	LevelWarning ColorConfig
	LevelError   ColorConfig
	LevelFatal   ColorConfig
	LevelDefault ColorConfig

	MessageDebug   ColorConfig
	MessageInfo    ColorConfig
	MessageWarning ColorConfig
	MessageError   ColorConfig
	MessageFatal   ColorConfig
	MessageDefault ColorConfig

	Extras ColorConfig
}

func (cc ColorConfig) Prepare(t ColorTheme, level, typ int) ColorConfig {
	cc.Foreground = cc.prepareColor(cc.Foreground, level, typ, t, true)
	cc.Background = cc.prepareColor(cc.Background, level, typ, t, false)

	return cc
}

func (cc ColorConfig) PrepareDefault(l *Logger) ColorConfig {
	return cc.Prepare(l.Theme, -1, ConfigTypeMessage)
}

func (cc ColorConfig) prepareColor(c, level, typ int, t ColorTheme, isForeground bool) int {
	if c == ColorLogLevel {
		if typ == ConfigTypeLevel {
			return ColorNone
		} else {
			conf := t.GetColorConfig(level, ConfigTypeMessage)

			if isForeground {
				return cc.prepareColor(conf.Foreground, level, typ, t, isForeground)
			} else {
				return cc.prepareColor(conf.Background, level, typ, t, isForeground)
			}
		}
	} else if c == ColorDefault {
		conf := t.GetColorConfig(-1, ConfigTypeLevel)

		if isForeground {
			return cc.prepareColor(conf.Foreground, level, typ, t, isForeground)
		} else {
			return cc.prepareColor(conf.Background, level, typ, t, isForeground)
		}
	} else if c == ColorDependBackground {
		if !isForeground {
			return ColorNone
		}

		col := cc.Background

		if col == ColorNone {
			return ColorNone
		} else if (col > 6 && col < 16) ||
			(col > 33 && col < 52) ||
			(col > 69 && col < 88) ||
			(col > 105 && col < 124) ||
			(col > 141 && col < 160) ||
			(col > 177 && col < 196) ||
			(col > 213 && col < 232) ||
			col > 244 {
			return 0
		} else {
			return 15
		}
	} else if c > 255 {
		return ColorNone
	}

	return c
}

func (t ColorTheme) GetColorConfig(level, typ int) ColorConfig {
	switch typ {
	case ConfigTypeMessage:
		switch level {
		case LogDebug:
			return t.MessageDebug
		case LogInfo:
			return t.MessageInfo
		case LogWarning:
			return t.MessageWarning
		case LogError:
			return t.MessageError
		case LogFatal:
			return t.MessageFatal
		}
		return t.MessageDefault
	case ConfigTypeLevel:
		switch level {
		case LogDebug:
			return t.LevelDebug
		case LogInfo:
			return t.LevelInfo
		case LogWarning:
			return t.LevelWarning
		case LogError:
			return t.LevelError
		case LogFatal:
			return t.LevelFatal
		}
		return t.LevelDefault
	}

	return NColorConfig(ColorNone, ColorNone)
}

func NColorConfig(foreground, background int) ColorConfig {
	return ColorConfig{
		Foreground: foreground,
		Background: background,
	}
}

const exampleText = "The quick brown fox jumps over the lazy dog"

func (t ColorTheme) Debug(l *Logger) {
	l.Debug(SMsg("SMsg: " + exampleText))
	l.Info(SMsg("SMsg: " + exampleText))
	l.Warning(SMsg("SMsg: " + exampleText))
	l.Error(SMsg("SMsg: " + exampleText))
	l.Fatal(SMsg("SMsg: " + exampleText))

	_ = l.RawPrint("\n")

	l.Debug(LMsg("LMsg: " + exampleText))
	l.Info(LMsg("LMsg: " + exampleText))
	l.Warning(LMsg("LMsg: " + exampleText))
	l.Error(LMsg("LMsg: " + exampleText))
	l.Fatal(LMsg("LMsg: " + exampleText))

	_ = l.RawPrint("\n")

	l.Info(CFMsg(22, "Some "), CBMsg(124, "Nice"), CFMsg(214, " Colors"))

	_ = l.RawPrint("\n")
}
