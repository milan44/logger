package logger

import (
	"fmt"
	"math/rand"
	"testing"
	"time"
)

func TestLogging(t *testing.T) {
	rand.Seed(time.Now().UnixNano())
	logger := NewLogger()

	fmt.Println(" - logger.Theme = ThemeDefault()")
	logger.Theme = ThemeDefault()
	logger.Theme.Debug(&logger)

	fmt.Println("\n - logger.Theme = ThemeBlocks()")
	logger.Theme = ThemeBlocks()
	logger.Theme.Debug(&logger)

	fmt.Println("\n - Random Colors")
	printWithRandomColors(exampleText, &logger)

	fmt.Println("\n - RGB8Bit2C")
	logger.Info(CBMsg(RGB8Bit2C(241, 166, 41), " #f1a629 "), SMsg(" "), CBMsg(RGB8Bit2C(33, 118, 117), " #217675 "), SMsg(" "), CBMsg(RGB8Bit2C(127, 76, 93), " #7f4c5d "))

	fmt.Println("\n - Gin Middleware (Random IP and status-code)")
	cycleRequests()

	fmt.Println("\n - Gin Logger (enableTrace=true)")
	g := NewGinStyleLogger(true)
	g.Test()

	fmt.Println("\n - Gin Logger (enableTrace=false)")
	g = NewGinStyleLogger(false)
	g.Test()

	fmt.Println("\n - Color Table")
	printColorTable(&logger)

	fmt.Println("\n - GetCallStack")
	call := GetCallStack()
	fmt.Println(call.String() + "\n")
	fmt.Println(call.ShortString() + "\n")
	fmt.Println(call.PathString() + "\n")
}

func printWithRandomColors(text string, logger *Logger) {
	runes := []rune(text)
	msgs := make([]ColoredMessage, 0)

	for _, r := range runes {
		msgs = append(msgs, CFMsg(RandomColor(), string(r)))
	}

	logger.Info(msgs...)
}
