package logger

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"path/filepath"
	"runtime"
	"strings"
	"time"
)

type GinLogger struct {
	*ShortLogger
	middleware gin.HandlerFunc
}

// QuickGinMiddleware is a shortcut for creating a short logger and getting its gin middleware
func QuickGinMiddleware() gin.HandlerFunc {
	l := NewGinStyleLogger(false)
	return l.GinMiddleware()
}

// GinMiddleware is a logger middleware for gin-gonic
func (l *GinLogger) GinMiddleware() gin.HandlerFunc {
	return l.middleware
}

func (l *GinLogger) initGinMiddleware() {
	l.middleware = func(c *gin.Context) {
		c.Next()

		l.LogRequest(c.Request.Method, c.ClientIP(), c.Request.URL.Path, c.Request.Header.Get("user-agent"), c.Writer.Status())
	}
}

// LogRequest logs a http request
func (l *GinLogger) LogRequest(method, clientIp, path, userAgent string, statusCode int) {
	msgs := make([]ColoredMessage, 0)

	msgs = append(msgs, CMsg(243, 234, fmt.Sprintf("%-33s ", l.Internal.FormatDate())))

	col := 52
	switch method {
	case "GET":
		col = 33
	case "HEAD":
		col = 141
	case "POST":
		col = 36
	case "PUT":
		col = 34
	case "DELETE":
		col = 160
	case "CONNECT":
		col = 45
	case "OPTIONS":
		col = 209
	case "TRACE":
		col = 162
	case "PATCH":
		col = 202
	}

	msgs = append(msgs, CMsg(col, 234, fmt.Sprintf("[%-7s] ", strings.ToLower(method))))
	msgs = append(msgs, CMsg(248, 234, fmt.Sprintf("[%-15s] ", clientIp)))

	col = 111
	if statusCode >= 100 && statusCode < 200 {
		col = 159
	} else if statusCode >= 200 && statusCode < 300 {
		col = 34
	} else if statusCode >= 300 && statusCode < 400 {
		col = 184
	} else if statusCode >= 400 && statusCode < 500 {
		col = 202
	} else if statusCode >= 500 {
		col = 124
	}

	/*
	 * 218 This is fine
	 * 418 I'm a teapot
	 * 420 Enhance Your Calm
	 */
	if statusCode == 218 || statusCode == 418 || statusCode == 420 {
		col = 210
	} else if http.StatusText(statusCode) == "" {
		col = 111
	}

	msgs = append(msgs, CMsg(col, 234, fmt.Sprintf("[%-3d] ", statusCode)))

	msgs = append(msgs, CMsg(248, 234, "- "+path))

	if userAgent != "" {
		msgs = append(msgs, CMsg(248, 234, " "+userAgent))
	}

	l.Internal.printMutex.Lock()
	l.Internal.Color(msgs...)
	_ = l.Internal.RawPrint("\n")
	l.Internal.printMutex.Unlock()
}

// NewGinStyleLogger returns a gin styled logger
func NewGinStyleLogger(enableTrace bool) GinLogger {
	_l := NewShortLogger()
	g := GinLogger{
		ShortLogger: &_l,
	}

	g.Internal.Theme = ThemeGin()
	g.Internal.DateFormat = time.RFC3339Nano

	g.MessageConverter = func(msg interface{}, _ int) ColoredMessage {
		return CMsg(248, 234, msg)
	}

	g.Internal.Print = func(l *Logger, date string, logLevel int, msg ...ColoredMessage) {
		c := l.Theme.GetColorConfig(logLevel, ConfigTypeLevel)

		msgs := make([]ColoredMessage, 0)

		msgs = append(msgs, CMsg(243, 234, fmt.Sprintf("%-33s ", date)))
		msgs = append(msgs, CMsg(c.Foreground, 234, fmt.Sprintf("[%-7s] ", strings.TrimSpace(strings.ToLower(formatLogLevel(logLevel))))))

		if enableTrace {
			_, file, line, ok := runtime.Caller(3)
			if !ok {
				file = "internal"
				line = 0
			} else {
				file = filepath.Base(file)
			}

			msgs = append(msgs, CMsg(111, 234, fmt.Sprintf("%s:%d", file, line)))
			msgs = append(msgs, CMsg(243, 234, " - "))
		}

		for _, m := range msg {
			m.Color = ColorConfig{
				Foreground: 248,
				Background: 234,
			}

			msgs = append(msgs, m)
		}

		l.Color(msgs...)
		_ = l.RawPrint("\n")
	}

	g.initGinMiddleware()

	return g
}
