package logger

import (
	"fmt"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
)

// StackEntry defines an entry in the caller stack
type StackEntry struct {
	File string `json:"file"`
	Func string `json:"func"`
	Line int    `json:"line"`
}

// CallStack defines the caller stack
type CallStack struct {
	Entries []StackEntry
}

// String formats a stack entry
func (s StackEntry) String() string {
	res := ""
	if s.Func != "" {
		res = s.Func + "()\n    "
	}

	res += s.File + ":" + strconv.Itoa(s.Line)

	return res
}

// GetCallStack returns the current call stack
func GetCallStack() CallStack {
	stack := CallStack{
		Entries: make([]StackEntry, 0),
	}

	s := 1
	for {
		pc, file, line, ok := runtime.Caller(s)
		fnc := runtime.FuncForPC(pc)

		if ok {
			file = filepath.Base(file)

			function := ""
			if fnc != nil {
				function = fnc.Name()
			}

			stack.Entries = append(stack.Entries, StackEntry{
				File: file,
				Line: line,
				Func: function,
			})
		} else {
			return stack
		}

		s++
	}
}

// String formats a call stack
func (c CallStack) String() string {
	lines := make([]string, 0)
	for _, entry := range c.Entries {
		lines = append(lines, entry.String())
	}

	if len(lines) == 0 {
		return "empty call stack"
	}

	return strings.Join(lines, "\n")
}

// ShortString formats a call stack into a one-line string using function names
func (c CallStack) ShortString() string {
	entries := make([]string, 0)
	for i := len(c.Entries) - 1; i >= 0; i-- {
		entry := c.Entries[i]

		if entry.Func == "" {
			entry.Func = "internal"
		}

		entries = append(entries, fmt.Sprintf("%s()", entry.Func))
	}

	if len(entries) == 0 {
		return "empty call stack"
	}

	return strings.Join(entries, " -> ")
}

// PathString formats a call stack into a one-line string using filenames and line-numbers
func (c CallStack) PathString() string {
	entries := make([]string, 0)
	for i := len(c.Entries) - 1; i >= 0; i-- {
		entry := c.Entries[i]

		entries = append(entries, fmt.Sprintf("%s:%d", entry.File, entry.Line))
	}

	if len(entries) == 0 {
		return "empty call stack"
	}

	return strings.Join(entries, " -> ")
}
