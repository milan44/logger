Very simple straight forward logger for go programs supporting all common operating systems (windows, linux, mac). You can disable color output with the UseColor function. You can change the log level using the SetLogLevel function.

```go
package main

import "gitlab.com/milan44/logger"

func main() {
    log := logger.NewGinStyleLogger(false)

    log.Debug("This is a Debug Message")
    log.Info("This is an Info Message")
    log.Warning("This is a Warning Message")
    log.Error("This is an Error Message")
    log.Fatal("This is a Fatal Message")
}
```

---

#### [`go test`](https://asciinema.org/a/425801)
![go_test](go_test.png)