package logger

import (
	"fmt"
	"runtime/debug"
)

// ShortLogger defines a shorthand for the main Logger (MessageConverter gets the message and the log level and has to return a colored Message)
type ShortLogger struct {
	Internal         Logger
	MessageConverter func(interface{}, int) ColoredMessage
}

// NewShortLogger creates a new ShortLogger
func NewShortLogger() ShortLogger {
	return ShortLogger{
		Internal: NewLogger(),
		MessageConverter: func(msg interface{}, _ int) ColoredMessage {
			return SMsg(msg)
		},
	}
}

// RenderStack renders the current stacktrace
func (l *ShortLogger) RenderStack() string {
	return string(debug.Stack())
}

// Printf short for logger.Log(LogLog, l.MessageConverter(fmt.Sprintf(format, data...), LogLog))
func (l *ShortLogger) Printf(format string, data ...interface{}) {
	l.Internal.Log(LogLog, l.MessageConverter(fmt.Sprintf(format, data...), LogLog))
}

// LogStack will log RenderStack with a given log level using l.MessageConverter
func (l *ShortLogger) LogStack(level int) {
	l.Internal.Log(level, l.MessageConverter(l.RenderStack(), level))
}

// Debug Short for logger.Log(LogDebug, logger.MessageConverter(msg))
func (l *ShortLogger) Debug(msg interface{}) {
	l.Internal.Log(LogDebug, l.MessageConverter(msg, LogDebug))
}

// Info Short for Log(LogInfo, logger.MessageConverter(msg))
func (l *ShortLogger) Info(msg interface{}) {
	l.Internal.Log(LogInfo, l.MessageConverter(msg, LogInfo))
}

// Warning Short for Log(LogWarning, logger.MessageConverter(msg))
func (l *ShortLogger) Warning(msg interface{}) {
	l.Internal.Log(LogWarning, l.MessageConverter(msg, LogWarning))
}

// Error Short for Log(LogError, logger.MessageConverter(msg))
func (l *ShortLogger) Error(msg interface{}) {
	l.Internal.Log(LogError, l.MessageConverter(msg, LogError))
}

// Fatal Short for Log(LogFatal, logger.MessageConverter(msg))
func (l *ShortLogger) Fatal(msg interface{}) {
	l.Internal.Log(LogFatal, l.MessageConverter(msg, LogFatal))
}

// DebugE short for printing the error message (err.Error()) using l.MessageConverter and the log level LogDebug
func (l *ShortLogger) DebugE(err error) {
	l.Debug(err.Error())
}

// InfoE short for printing the error message (err.Error()) using l.MessageConverter and the log level LogInfo
func (l *ShortLogger) InfoE(err error) {
	l.Info(err.Error())
}

// WarningE short for printing the error message (err.Error()) using l.MessageConverter and the log level LogWarning
func (l *ShortLogger) WarningE(err error) {
	l.Warning(err.Error())
}

// ErrorE short for printing the error message (err.Error()) using l.MessageConverter and the log level LogError
func (l *ShortLogger) ErrorE(err error) {
	l.Error(err.Error())
}

// FatalE short for printing the error message (err.Error()) using l.MessageConverter and the log level LogFatal
func (l *ShortLogger) FatalE(err error) {
	l.Fatal(err.Error())
}

// MustPanic will call the logger.Panic function ONLY if the error is not nil
func (l *ShortLogger) MustPanic(err error) {
	if err != nil {
		l.Panic(err)
	}
}

// Panic will FatalE(err) and then panic(err)
func (l *ShortLogger) Panic(err error) {
	l.FatalE(err)
	panic(err)
}

// Test will print examples for all log levels
func (l *ShortLogger) Test() {
	l.Debug("The quick brown fox jumps over the lazy dog")
	l.Info("The quick brown fox jumps over the lazy dog")
	l.Warning("The quick brown fox jumps over the lazy dog")
	l.Error("The quick brown fox jumps over the lazy dog")
	l.Fatal("The quick brown fox jumps over the lazy dog")
}
