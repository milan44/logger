package logger

const (
	LogDebug   = 0
	LogLog     = 1
	LogInfo    = 2
	LogWarning = 3
	LogError   = 4
	LogFatal   = 5
)

func formatLogLevel(level int) string {
	switch level {
	case LogDebug:
		return "DEBUG"
	case LogLog:
		return " LOG "
	case LogInfo:
		return " INFO"
	case LogWarning:
		return " WARN"
	case LogError:
		return "ERROR"
	case LogFatal:
		return "FATAL"
	}
	return "?????"
}
