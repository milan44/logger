package logger

func ThemeDefault() ColorTheme {
	return ColorTheme{
		LevelDebug:   NColorConfig(243, ColorNone),
		LevelInfo:    NColorConfig(75, ColorNone),
		LevelWarning: NColorConfig(166, ColorNone),
		LevelError:   NColorConfig(124, ColorNone),
		LevelFatal:   NColorConfig(196, ColorNone),
		LevelDefault: NColorConfig(253, ColorNone),

		MessageDebug:   NColorConfig(243, ColorNone),
		MessageInfo:    NColorConfig(117, ColorNone),
		MessageWarning: NColorConfig(172, ColorNone),
		MessageError:   NColorConfig(166, ColorNone),
		MessageFatal:   NColorConfig(196, ColorNone),
		MessageDefault: NColorConfig(253, ColorNone),

		Extras: NColorConfig(8, ColorNone),
	}
}

func ThemeBlocks() ColorTheme {
	return ColorTheme{
		LevelDebug:   NColorConfig(247, 237),
		LevelInfo:    NColorConfig(255, 25),
		LevelWarning: NColorConfig(52, 202),
		LevelError:   NColorConfig(255, 124),
		LevelFatal:   NColorConfig(255, 196),
		LevelDefault: NColorConfig(ColorNone, 253),

		MessageDebug:   NColorConfig(243, ColorNone),
		MessageInfo:    NColorConfig(117, ColorNone),
		MessageWarning: NColorConfig(172, ColorNone),
		MessageError:   NColorConfig(166, ColorNone),
		MessageFatal:   NColorConfig(196, ColorNone),
		MessageDefault: NColorConfig(253, ColorNone),

		Extras: NColorConfig(8, ColorNone),
	}
}

func ThemeGin() ColorTheme {
	return ColorTheme{
		LevelDebug:   NColorConfig(244, 234),
		LevelInfo:    NColorConfig(45, 234),
		LevelWarning: NColorConfig(208, 234),
		LevelError:   NColorConfig(160, 234),
		LevelFatal:   NColorConfig(196, 234),
		LevelDefault: NColorConfig(36, 234),

		MessageDebug:   NColorConfig(248, 234),
		MessageInfo:    NColorConfig(248, 234),
		MessageWarning: NColorConfig(248, 234),
		MessageError:   NColorConfig(248, 234),
		MessageFatal:   NColorConfig(248, 234),
		MessageDefault: NColorConfig(248, 234),

		Extras: NColorConfig(248, 234),
	}
}
