package logger

import "time"

func (l *Logger) FormatDate() string {
	return time.Now().Format(l.DateFormat)
}
