module gitlab.com/milan44/logger

go 1.14

require (
	github.com/gin-gonic/gin v1.7.2
	github.com/gookit/color v1.3.6
)
