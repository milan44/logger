package logger

import (
	"io"
	"os"
	"runtime/debug"
	"sync"
)

type Logger struct {
	DateFormat string
	LogLevel   int
	UseColors  bool
	Theme      ColorTheme
	Print      func(l *Logger, date string, logLevel int, msg ...ColoredMessage)
	Output     io.Writer

	outputFile  *os.File
	coloredFile bool

	printMutex sync.Mutex
}

// NewLogger returns a new logger with the default values
func NewLogger() Logger {
	return Logger{
		DateFormat: "[02.01.2006 - 15:04:05]",
		LogLevel:   0,
		UseColors:  true,
		Theme:      ThemeDefault(),
		Print: func(l *Logger, date string, logLevel int, msg ...ColoredMessage) {
			c := l.Theme.GetColorConfig(logLevel, ConfigTypeLevel)
			ex := l.Theme.Extras

			l.Color(CCMsg(ex, date))
			_ = l.RawPrint(" ")
			l.Color(CCMsg(c, formatLogLevel(logLevel)))
			l.Color(CCMsg(ex, ": "))
			l.Color(msg...)
		},
		Output:      os.Stdout,
		outputFile:  nil,
		coloredFile: true,
	}
}

// ColoredMessage defines part of or a complete message with the color it should be printed in
type ColoredMessage struct {
	Color   ColorConfig
	Message string
}

// SetOutputFile opens/creates a file and sets is as the loggers output
func (l *Logger) SetOutputFile(path string, createIfNotExists bool) error {
	flags := os.O_WRONLY | os.O_APPEND
	if createIfNotExists {
		flags |= os.O_CREATE
	}

	f, err := os.OpenFile(path, flags, 0777)
	if err != nil {
		return err
	}

	l.Output = f
	l.outputFile = f
	return nil
}

// CloseOutput closes the file used to output logs if there is one
func (l *Logger) CloseOutput() error {
	if l.outputFile != nil {
		err := l.outputFile.Close()
		l.outputFile = nil

		return err
	}
	return nil
}

// UseColorInFile defines if color escape codes should be written to the output file
func (l *Logger) UseColorInFile(v bool) {
	l.coloredFile = v
}

func terminateMsg(msg []ColoredMessage) []ColoredMessage {
	msg = append(msg, ColoredMessage{
		Color:   msg[len(msg)-1].Color,
		Message: "\n",
	})

	return msg
}

// Log is the main log function which prints a list of ColoredMessage structs with the log prefix
func (l *Logger) Log(level int, msg ...ColoredMessage) {
	if level < l.LogLevel {
		return
	}

	for i, m := range msg {
		msg[i].Color = m.Color.Prepare(l.Theme, level, ConfigTypeMessage)
	}

	l.printMutex.Lock()
	l.Print(l, l.FormatDate(), level, msg...)
	l.printMutex.Unlock()
}

// LogPrint Short for Log(LogLog, msg...)
func (l *Logger) LogPrint(msg ...ColoredMessage) {
	l.Log(LogLog, msg...)
}

// RenderStack renders the current stacktrace
func (l *Logger) RenderStack() string {
	return string(debug.Stack())
}

// LogStack will log RenderStack with a given log level
func (l *Logger) LogStack(level int) {
	l.Log(level, SMsg(l.RenderStack()))
}

// Debug Short for Log(LogDebug, msg...)
func (l *Logger) Debug(msg ...ColoredMessage) {
	l.Log(LogDebug, terminateMsg(msg)...)
}

// Info Short for Log(LogInfo, msg...)
func (l *Logger) Info(msg ...ColoredMessage) {
	l.Log(LogInfo, terminateMsg(msg)...)
}

// Warning Short for Log(LogWarning, msg...)
func (l *Logger) Warning(msg ...ColoredMessage) {
	l.Log(LogWarning, terminateMsg(msg)...)
}

// Error Short for Log(LogError, msg...)
func (l *Logger) Error(msg ...ColoredMessage) {
	l.Log(LogError, terminateMsg(msg)...)
}

// Fatal Short for Log(LogFatal, msg...)
func (l *Logger) Fatal(msg ...ColoredMessage) {
	l.Log(LogFatal, terminateMsg(msg)...)
}

// DebugE short for printing the error message (err.Error()) with no color and the log level LogDebug
func (l *Logger) DebugE(err error) {
	l.Debug(SMsg(err.Error()))
}

// InfoE short for printing the error message (err.Error()) with no color and the log level LogInfo
func (l *Logger) InfoE(err error) {
	l.Info(SMsg(err.Error()))
}

// WarningE short for printing the error message (err.Error()) with no color and the log level LogWarning
func (l *Logger) WarningE(err error) {
	l.Warning(SMsg(err.Error()))
}

// ErrorE short for printing the error message (err.Error()) with no color and the log level LogError
func (l *Logger) ErrorE(err error) {
	l.Error(SMsg(err.Error()))
}

// FatalE short for printing the error message (err.Error()) with no color and the log level LogFatal
func (l *Logger) FatalE(err error) {
	l.Fatal(SMsg(err.Error()))
}

// MustPanic will call the logger.Panic function ONLY if the error is not nil
func (l *Logger) MustPanic(err error) {
	if err != nil {
		l.Panic(err)
	}
}

// Panic will FatalE(err) and then panic(err)
func (l *Logger) Panic(err error) {
	l.FatalE(err)
	panic(err)
}
